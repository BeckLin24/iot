#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

// This is for git version tracking.  Safe to ignore
#ifdef VERSION_H
#include "version.h"
#else
#define __TAG__ "1.0.03-0001"
#endif
enum SENSOR_STATUS {
	SENSOR_STATUS_OFF,
	SENSOR_STATUS_ON,
	SENSOR_STATUS_RESET
};

enum ELEMENT_TYPE {
	ELEMENT_TYPE_PIPE = 0x00,
	ELEMENT_TYPE_EVENT1,
	ELEMENT_TYPE_RET1,
	ELEMENT_TYPE_EVENT2,
	ELEMENT_TYPE_RET2,
	ELEMENT_TYPE_STATUS,
	ELEMENT_TYPE_ACTION,
	ELEMENT_TYPE_TIMESTAMP,
	ELEMENT_TYPE_MAX
};

enum SENSOR_TYPE {
	SENSOR_TYPE_UNKNOWN     = 0, // 2 << 0
	SENSOR_TYPE_FLAME       = 1, // 2 << 1
	SENSOR_TYPE_GAS         = 2, // 2 << 2
	SENSOR_TYPE_CO          = 3, // 2 << 3
	SENSOR_TYPE_WATER       = 4, // 2 << 4
	SENSOR_TYPE_NOISE       = 5, // 2 << 5
	SENSOR_TYPE_TEMPERATURE = 6, // 2 << 6
	SENSOR_TYPE_RFID        = 7, // 2 << 7
	SENSOR_TYPE_SHOCK       = 8, // 2 << 8
	SENSOR_TYPE_INTRUSION   = 9, // 2 << 9
	SENSOR_TYPE_MAGETIC     = 10,// 2 << 10
	SENSOR_TYPE_PHOTOCELL   = 11,// 2 << 11
	SENSOR_TYPE_REVERSE1 = 12,   // 2 << 12
	SENSOR_TYPE_REVERSE2 = 13,   // 2 << 13
	SENSOR_TYPE_REVERSE3 = 14,   // 2 << 14
	SENSOR_TYPE_MAX      = 15    // 2 << 15
};

enum ACTION_TYPE {
	ACTION_TYPE_DONTCARE	= 0x00,
	ACTION_TYPE_BEE			= 0x01,
	ACTION_TYPE_ALARM		= 0x02,
	ACTION_TYPE_MAIL		= 0x04,
	ACTION_TYPE_PHONE		= 0x08,
	ACTION_TYPE_PHOTO		= 0x10,
	ACTION_TYPE_RECORD		= 0x20,
	ACTION_TYPE_MAP			= 0x40
};

struct Payload {
	char nodeID[12];
	int event1_type;
	int return1_value;
	int event2_type;
	int return2_value;
	SENSOR_STATUS sensor_status;
	ACTION_TYPE	sensor_action;
	unsigned int time_stamp;
};

struct ActionReponse {
	ACTION_TYPE warning_type_;
	int warning_;
	ACTION_TYPE alarm_type_;
	int alarm_;
	ACTION_TYPE urgent_type;
	int urgent_;
};

struct Sensor {
	int channe_;				// rf24 channel id
	int pipe_id_;				// pipe#
	int sensor_value_;			// sensor value
	unsigned int time_stamp_;	// time stamp
	SENSOR_TYPE sensor_type_;	// sensor type
	SENSOR_STATUS status_;		// sensor status
	ActionReponse action_reponse_;// event reponse
};

// rf channel number
const int CHANNELID = 60;
//#define RF_SETUP 0x17

// Radio pipe addresses for the 2 nodes to communicate.
// const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
   const uint64_t pipes[2] = { 0xF0F0F0F0E2LL, 0xF0F0F0F0D2LL };
// const uint64_t pipes[2] = { 0xF0F0F0F0E3LL, 0xF0F0F0F0D2LL };
// const uint64_t pipes[2] = { 0xF0F0F0F0F1LL, 0xF0F0F0F0D2LL };
// const uint64_t pipes[2] = { 0xF0F0F0F0F2LL, 0xF0F0F0F0D2LL };
// Pipe0 is F0F0F0F0D2 ( same as reading pipe )

// rf timer & its duration
const int TIMEOUT = 1000;
unsigned long last_time = 0;

// sensor
//const int BUZZER_PIN = 7;
const int CO_PIN = A0;

/// GLOBAL VARIABLES
// Set up nRF24L01 radio on SPI pin for CE, CSN
RF24 radio(9,10);
char receivePayload[32] = {0};
char outBuffer[32] = {0};

unsigned long g_coResult = 0; // range: [0:1024]
static int  g_coStatus = (int)SENSOR_STATUS_OFF;
static int  g_coAction = (int)ACTION_TYPE_DONTCARE;
static int  g_coThreshold = 40; // initialize value

void setup_RF_2_RPI(void)
{
  printf_begin();
  printf_P(PSTR("VERSION: " __TAG__ "\n\r"));

  radio.begin();
  radio.enableDynamicPayloads();
  radio.setAutoAck(1);
  radio.setRetries(15,15);

  // Setup default radio settings  
  radio.setDataRate(RF24_1MBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(CHANNELID);
  radio.setCRCLength(RF24_CRC_16);
  radio.openWritingPipe(pipes[0]); 
  radio.openReadingPipe(1,pipes[1]);
  // Send only, ignore listening mode
  //radio.startListening();

  radio.printDetails(); 
}

void rf2rpi(void)
{
  bool updateStatus = false;
  bool done = false;
  while (radio.available() && !done ) 
  {
    uint8_t len = radio.getDynamicPayloadSize();
    done = radio.read( receivePayload, len); 
    printf(" inBuffer: %s\n\r", receivePayload);

    if (done)
    {
      int idx = 0;
      char *token[ELEMENT_TYPE_MAX];
      char *p = receivePayload;
      char *str;
      while ((str = strtok_r(p, ",", &p)) != NULL)
      {
        token[idx] = str;
        idx++;
      }

      if (atoi(token[ELEMENT_TYPE_EVENT1]) == (int)SENSOR_TYPE_CO)
      {
        // update sensor threshold
	int coT = atoi(token[ELEMENT_TYPE_RET1]);
        if (coT < 0)
        {
          g_coThreshold = -coT;
          g_coAction = atoi(token[ELEMENT_TYPE_ACTION]);
          g_coStatus = (int)SENSOR_STATUS_RESET;
          Serial.println("RESET!!! -- CO");
        }
        else
        {
          g_coStatus = (int)SENSOR_STATUS_ON;
        }
        
        // debug message
        if (1)
        {
          Serial.print( "nodeID: ");        Serial.println(token[ELEMENT_TYPE_PIPE]);
          Serial.print( "event1_type: ");   Serial.println(token[ELEMENT_TYPE_EVENT1]);
          Serial.print( "sensiti1_value: "); Serial.println(token[ELEMENT_TYPE_RET1]);
          Serial.print( "event2_type: ");   Serial.println(token[ELEMENT_TYPE_EVENT2]);
          Serial.print( "sensiti2_value: "); Serial.println(token[ELEMENT_TYPE_RET2]);
          Serial.print( "status: ");        Serial.println(token[ELEMENT_TYPE_STATUS]);
          Serial.print( "action: ");        Serial.println(token[ELEMENT_TYPE_ACTION]);
          Serial.print( "time_stamp: ");    Serial.println(token[ELEMENT_TYPE_TIMESTAMP]);
        }
      }
    }
    delay(16);
  } // end of while 

  // reset output buffer
  memset(outBuffer, 0, sizeof(outBuffer));

  Payload pkg;
  pkg.event1_type     = (int)SENSOR_TYPE_CO;
  pkg.return1_value   = (int)g_coResult;
  pkg.event2_type     = (int)SENSOR_TYPE_UNKNOWN;
  pkg.return2_value   = 0;
  pkg.sensor_status   = (SENSOR_STATUS)g_coStatus;
  pkg.sensor_action   = (ACTION_TYPE)g_coAction;
  pkg.time_stamp      = millis();
  
  char temp[32]={0};
  if (pipes[0] == 0xF0F0F0F0E1LL)
    sprintf(pkg.nodeID,"%0d",  1); 
  else if (pipes[0] == 0xF0F0F0F0E2LL)
    sprintf(pkg.nodeID,"%0d",  2);
  else if (pipes[0] == 0xF0F0F0F0E3LL)
    sprintf(pkg.nodeID,"%0d",  3);
  else if (pipes[0] == 0xF0F0F0F0F1LL)
    sprintf(pkg.nodeID,"%0d",  4);
  else if (pipes[0] == 0xF0F0F0F0FLL)
    sprintf(pkg.nodeID,"%0d",  5);
  else
    sprintf(pkg.nodeID,"%0d",  0);
  strcat(outBuffer,pkg.nodeID); strcat(outBuffer,",");
  
  sprintf(temp,"%02d",  pkg.event1_type);  
  strcat(outBuffer,temp);  strcat(outBuffer,",");
  
  sprintf(temp,"%03d",  pkg.return1_value);
  strcat(outBuffer,temp);  strcat(outBuffer,",");
  
  sprintf(temp,"%02d",  pkg.event2_type);  
  strcat(outBuffer,temp);  strcat(outBuffer,",");
  
  sprintf(temp,"%03d",  pkg.return2_value);
  strcat(outBuffer,temp);  strcat(outBuffer,",");
  
  sprintf(temp,"%01d",  pkg.sensor_status);
  strcat(outBuffer,temp); strcat(outBuffer,",");
  
  sprintf(temp,"%05d",  pkg.sensor_action);
  strcat(outBuffer,temp);  strcat(outBuffer,",");
  
  sprintf(temp,"%04d",  pkg.time_stamp%10000);
  strcat(outBuffer,temp); /*strcat(outBuffer,0);*/
    
  printf("outBuffer: %s length: %d",outBuffer, strlen(outBuffer));

  {
    // Stop listening and write to radio 
    radio.stopListening();

    // Send outBuffer to hub(rpi)
    if ( radio.write( outBuffer, strlen(outBuffer)) ) 
    {
      printf("\t Send successful\n\r"); 
    }
    else 
    {
      // turn off co sensor
      g_coStatus = (int)SENSOR_STATUS_OFF;
      printf("\t Send failed\n\r");
    }

    radio.startListening();
  } 
}

// FUNCTIONS 
void setup_sensor()
{
  //pinMode(BUZZER_PIN,OUTPUT);
  pinMode(CO_PIN,INPUT);
}

void co_sensor()
{
  int val = analogRead(CO_PIN);
  if(val > g_coThreshold)
  {
    Serial.print("[ALARM] - CO!!!   ");   Serial.println(val, DEC);

    // to do action
    Serial.print(g_coAction, DEC);
    if (g_coAction == ACTION_TYPE_DONTCARE)
      Serial.println("[DontCare]!!!");
    else if (g_coAction == ACTION_TYPE_BEE)
      Serial.println("[Bee]!!!");
    else if (g_coAction == ACTION_TYPE_ALARM)
      Serial.println("[ALARM]!!!");
    else if (g_coAction == ACTION_TYPE_MAIL)
      Serial.println("[MAIL]!!!");  
    else if (g_coAction == ACTION_TYPE_PHONE)
      Serial.println("[PHONE]!!!");
    else if (g_coAction == ACTION_TYPE_PHOTO)
      Serial.println("[PHOTO]!!!");
    else if (g_coAction == ACTION_TYPE_RECORD)
      Serial.println("[RECORD]!!!");
    else if (g_coAction == ACTION_TYPE_MAP)
      Serial.println("[MAP]!!!");
    else
      Serial.println("[BUG]!!!");
  } 
  
  // save value
  g_coResult = val;
}

void setup(void)
{
  Serial.begin(9600);

  // seteup sensor & rf24
  setup_sensor();
  setup_RF_2_RPI();

  last_time = millis();
}

void loop(void)
{
  // query sensor value
  co_sensor();

  // send results to rpi
  if (millis() - last_time > TIMEOUT)
  {
    rf2rpi();
    last_time = millis();
  }
}
